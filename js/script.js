function isNumberInteger(value) {
  return Number.isInteger(value);
}

let userInput;
do {
  userInput = parseFloat(prompt("Input number:"));
} while (!isNumberInteger(userInput));

let isFinded = false;
for (let i = 0; i <= userInput; i++) {
  if (i % 5 == 0 && i !== 0) {
    isFinded = true;
    console.log(i);
  }
}
if (!isFinded) {
  console.log("Sorry, no numbers");
}

function isNumberValid(value) {
  return value !== null && value !== "" && !isNaN(value);
}

let secondUserInput;
do {
  userInput = parseInt(prompt("Input first number:"));
  secondUserInput = parseInt(prompt("Input second number:"));
} while (!isNumberValid(userInput) || !isNumberValid(secondUserInput));

function maxNumber(firstValue, secondValue) {
  if (firstValue > secondValue) return [firstValue, secondValue];
  else return [secondValue, firstValue];
}

let max = maxNumber(userInput, secondUserInput)[0];
let min = maxNumber(userInput, secondUserInput)[1];
for (let i = min; i <= max; i++) {
  for (let y = 1; y <= i; y++) {
    if (i % y === 0) {
      if (y !== 1 && y !== i) break;
      else if (y == i) console.log(i);
    }
  }
}
